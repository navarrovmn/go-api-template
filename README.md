# Go API Template

This is a boilerplate template for creating API's using GoLang. This template uses gin-gonic as a WEB framework. Using another may need to adapt some files or structure. 

The entrypoint for this API is the main.go file at root. The other directories are explained below.

## App

Here you have your application related files. It consists in at least:

* url_mappings.go: Reponsible for structuring app's paths to a controller; 
* application.go: Responsible for starting the app and routes;

## Controllers

Contains folders for each controller. If you have an entity called user or todos, each one would be a folder with it's filed (like user_controller.go or todo_controller.go). 

For contained applications using Amazon, it is a good idea to have a Ping controller to which AWS will check if the application is up and running.

## Services

This is where your business logic should be. Anything related to what you want to do with your data and how you want to do it are described here.
Using the example above, you could have a user folder with user_services.go to create your user logic.  

## Domain

Your entities must be here, if you are using DAO pattern. You can have a user_dao.go to describe your User type and how to access your user table for any operations you might need on your service.

## DataSources

This is where your communication with your database should be. Each database might be a different folder and have it's own associate files to handle database creation and connection.

## Utils

Anything you might need that doesn't fit into any folder above you can put here.



