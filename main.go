package main

import (
	"github.com/navarrovmn/bookstore_users-api/app"
)

func main() {
	app.StartApplication()
}